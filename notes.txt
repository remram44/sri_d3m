Build distributable:
   python setup.py sdist

Install Locally:
   pip3 install dist/sri-0.1.5.tar.gz --user --no-deps

Run Test:
   ./run_tests.py

Upload:
   twine upload dist/*.tar.gz


python3 -m sri.pipelines.output_primitive
--prediction d3m.primitives.classification.bert_classifier.DistilBertTextClassification
/Users/daraghhartnett/Projects/D3M/neural_text/code/sri_d3m/pipelines/primitives/SRI/d3m.primitives.classification.bert_classifier.DistilBertTextClassification/1.9.6/pipelines
/Users/daraghhartnett/Projects/D3M/neural_text/code/sri_d3m/pipelines/primitives/SRI/d3m.primitives.classification.bert_classifier.DistilBertTextClassification/1.9.6/pipeline_runs

DATASET_HOME = "/Users/daraghhartnett/Projects/D3M/data/datasets/seed_datasets_current"

