#!/usr/bin/env python3

import os
import re
import sys
import unittest

TEST_DIR = os.path.join('tests')
DATA_DIR = os.path.join(TEST_DIR, 'data')

# Return a list of unittest.TestCase
def collectTests(suite, testCases = []):
    if (isinstance(suite, unittest.TestCase)):
        testCases.append(suite)
        return testCases

    if (not isinstance(suite, unittest.suite.TestSuite)):
        raise ValueError("Unknown test type: %s" % (str(type(suite))))

    for testObject in suite:
        collectTests(testObject, testCases)

    return testCases

def main(pattern = None):
    runner = unittest.TextTestRunner(verbosity = 3)
    discoveredSuite = unittest.TestLoader().discover(TEST_DIR)
    testCases = collectTests(discoveredSuite)

    tests = unittest.suite.TestSuite()

    for testCase in testCases:
        if (isinstance(testCase, unittest.loader._FailedTest)):
            print('Failed to load test: %s' % (testCase.id()))
            print(testCase._exception)
            continue

        if (pattern is None or re.search(pattern, testCase.id())):
            tests.addTest(testCase)
        else:
            print("Skipping %s because of match pattern." % (testCase.id()))

    if (not runner.run(tests).wasSuccessful()):
        sys.exit(1)

if __name__ == '__main__':
    if (len(sys.argv) > 2):
        print("USAGE: python %s [test pattern]" % (sys.argv[0]))
        sys.exit(1)

    pattern = None
    if (len(sys.argv) == 2):
        pattern = sys.argv[1]

    main(pattern)
