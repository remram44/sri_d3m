# Tests pipeline construction and running.
# Pipeline tests require that the primitives are installed on the local machine.

import os
import sys
import time
import unittest

import d3m.container
import d3m.metadata
import d3m.runtime

import sri.pipelines.all
from sri.pipelines import base
from sri.pipelines import datasets
from tests import util

class TestPipeline(unittest.TestCase):
    def _test_pipeline(self, pipeline, dataset_name):
        # Load problem and data.
        path = 'file://{}'.format(util.getTrainProblemPath(dataset_name))
        problem = d3m.metadata.problem.Problem.load(path)

        path = 'file://{}'.format(util.getTrainPath(dataset_name))
        train_dataset = d3m.container.dataset.Dataset.load(dataset_uri = path)

        path = 'file://{}'.format(util.getTestPath(dataset_name))
        test_dataset = d3m.container.dataset.Dataset.load(dataset_uri = path)

        # Load the pipeline.
        pipeline_description = d3m.metadata.pipeline.Pipeline.from_json(string_or_file = pipeline.get_json())

        # Create the runtime.
        runtime = d3m.runtime.Runtime(
                pipeline = pipeline_description,
                problem_description = problem,
                context = d3m.metadata.base.Context.TESTING,
        )

        # Fitting on training data.
        fit_results = runtime.fit(inputs = [train_dataset])
        fit_results.check_success()

        # Producing on test data.
        produce_results = runtime.produce(inputs = [test_dataset])
        produce_results.check_success()

        pipeline.assert_result(self, produce_results.values, dataset_name, util.getScoreBaseDir(dataset_name))

# Set up the test methods for each pipeline/dataset combo.
# We want these as methods and not subtests so we can skip with regex.
def _make_pipeline_test(pipeline, dataset):
    def __method(self):
        start_time = time.time()

        self._test_pipeline(pipeline, dataset)

        total_time = time.time() - start_time
        print('(run in %d seconds) ' % (int(total_time)), end = '', file = sys.stderr)

    return __method

if (base.EVAL_PREDICTIONS):
    print('Found NIST evaluation package, prediction pipeline results will be validated.')

for pipeline_class in sri.pipelines.all.get_pipelines(test = True):
    pipeline = pipeline_class()

    for dataset in pipeline.get_datasets():
        test_name = 'test_pipeline_%s_%s' % (pipeline_class.__name__, dataset)
        setattr(TestPipeline, test_name, _make_pipeline_test(pipeline, dataset))
